package com.dating.koov.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;
import com.dating.koov.model.UserEntity;
import com.dating.koov.preference.PrefConst;
import com.dating.koov.preference.Preference;

public class SignupVerifyCodeActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_back_inInputVeriCodeActivity;

    UserEntity _user = new UserEntity();

    TextView ui_txv_phone_inInputVeriCodeActivity;

    EditText ui_edt_veri_code1, ui_edt_veri_code2, ui_edt_veri_code3, ui_edt_veri_code4, ui_edt_veri_code5, ui_edt_veri_code6;

    TextView ui_txv_complete_inInputVeriCodeActivity;
    private String verifyCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_verify_code);

        _user = (UserEntity)getIntent().getSerializableExtra(Constants.USER);

        loadLayout();
    }

    private void loadLayout() {

        ui_lyt_back_inInputVeriCodeActivity = (LinearLayout)findViewById(R.id.lyt_back_inInputVeriCodeActivity);
        ui_lyt_back_inInputVeriCodeActivity.setOnClickListener(this);

        ui_txv_phone_inInputVeriCodeActivity = (TextView)findViewById(R.id.txv_phone_inInputVeriCodeActivity);
        ui_txv_phone_inInputVeriCodeActivity.setText(_user.getPhone());

        ui_txv_complete_inInputVeriCodeActivity = (TextView)findViewById(R.id.txv_complete_inInputVeriCodeActivity);

        ui_edt_veri_code1 = (EditText)findViewById(R.id.edt_veri_code1);
        ui_edt_veri_code1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (b==true) {

                    ui_edt_veri_code1.setText("");
                    ui_txv_complete_inInputVeriCodeActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_complete_inInputVeriCodeActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_complete_inInputVeriCodeActivity.setOnClickListener(null);

                    ui_edt_veri_code2.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
                    ui_edt_veri_code2.setText("");

                    ui_edt_veri_code3.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
                    ui_edt_veri_code3.setText("");

                    ui_edt_veri_code4.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
                    ui_edt_veri_code4.setText("");

                    ui_edt_veri_code5.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
                    ui_edt_veri_code5.setText("");

                    ui_edt_veri_code6.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
                    ui_edt_veri_code6.setText("");
                }
            }
        });
        ui_edt_veri_code1.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (ui_edt_veri_code1.getText().length() == 1){

                    ui_edt_veri_code2.requestFocus();
                    ui_txv_complete_inInputVeriCodeActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_complete_inInputVeriCodeActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_complete_inInputVeriCodeActivity.setOnClickListener(SignupVerifyCodeActivity.this);

                } else {

                    ui_txv_complete_inInputVeriCodeActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_complete_inInputVeriCodeActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_complete_inInputVeriCodeActivity.setOnClickListener(null);
                }

                return false;
            }
        });

        ui_edt_veri_code2 = (EditText)findViewById(R.id.edt_veri_code2);
        ui_edt_veri_code2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b==true){
                    ui_edt_veri_code2.setBackgroundResource(R.drawable.bg_input_veri_code_active);
                }
            }
        });
        ui_edt_veri_code2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (ui_edt_veri_code2.getText().length() ==1)  ui_edt_veri_code3.requestFocus();

                return false;
            }
        });

        ui_edt_veri_code3 = (EditText)findViewById(R.id.edt_veri_code3);
        ui_edt_veri_code3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b== true)
                ui_edt_veri_code3.setBackgroundResource(R.drawable.bg_input_veri_code_active);
            }
        });
        ui_edt_veri_code3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (ui_edt_veri_code3.getText().length() ==1)
                    ui_edt_veri_code4.requestFocus();
                return false;
            }
        });
        ui_edt_veri_code4 = (EditText)findViewById(R.id.edt_veri_code4);
        ui_edt_veri_code4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b==true)
                    ui_edt_veri_code4.setBackgroundResource(R.drawable.bg_input_veri_code_active);
            }
        });
        ui_edt_veri_code4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (ui_edt_veri_code4.getText().length() ==1)
                    ui_edt_veri_code5.requestFocus();
                return false;
            }
        });

        ui_edt_veri_code5 = (EditText)findViewById(R.id.edt_veri_code5);
        ui_edt_veri_code5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {

                if (b==true){
                    ui_edt_veri_code5.setBackgroundResource(R.drawable.bg_input_veri_code_active);
                }
            }
        });
        ui_edt_veri_code5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (ui_edt_veri_code5.getText().length() ==1)
                    ui_edt_veri_code6.requestFocus();
                return false;
            }
        });
        ui_edt_veri_code6 = (EditText)findViewById(R.id.edt_veri_code6);
        ui_edt_veri_code6.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b==true){
                    ui_edt_veri_code6.setBackgroundResource(R.drawable.bg_input_veri_code_active);
                }
            }
        });

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_veri_code1.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void goPrev() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Tips");
        alertDialog.setMessage("gadshklahgoapeghoaphgpadshjpa?");

        alertDialog.setPositiveButton("Wait", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                dialogInterface.cancel();
            }
        });

        alertDialog.setNegativeButton("Return", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                finish();
                overridePendingTransition(R.anim.push_out_right, R.anim.pull_in_left);

                dialogInterface.cancel();
            }
        });

        alertDialog.show();
    }

    private void Initial() {
        ui_edt_veri_code1.setText("");
        ui_edt_veri_code1.setBackgroundResource(R.drawable.bg_input_veri_code_active);

        ui_edt_veri_code2.setText("");
        ui_edt_veri_code2.setBackgroundResource(R.drawable.bg_input_veri_code_normal);

        ui_edt_veri_code3.setText("");
        ui_edt_veri_code3.setBackgroundResource(R.drawable.bg_input_veri_code_normal);

        ui_edt_veri_code4.setText("");
        ui_edt_veri_code4.setBackgroundResource(R.drawable.bg_input_veri_code_normal);

        ui_edt_veri_code5.setText("");
        ui_edt_veri_code5.setBackgroundResource(R.drawable.bg_input_veri_code_normal);

        ui_edt_veri_code6.setText("");
        ui_edt_veri_code6.setBackgroundResource(R.drawable.bg_input_veri_code_normal);
    }


    public String getVerifyCode() {

        verifyCode = ui_edt_veri_code1.getText().toString().trim() + ui_edt_veri_code2.getText().toString().trim() + ui_edt_veri_code3.getText().toString().trim()
                + ui_edt_veri_code4.getText().toString().trim() + ui_edt_veri_code5.getText().toString().trim() + ui_edt_veri_code6.getText().toString().trim();
        verifyCode = verifyCode.toString().trim();

        return verifyCode;
    }


    private void gotoContactActivity() {

        Intent intent = new Intent(this, ContactActivity.class);
        startActivity(intent);
    }

    private void gotoSuccessRegister() {

        Preference.getInstance().put(this, PrefConst.LOGIN_SUCCESS, true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.lyt_back_inInputVeriCodeActivity:

                goPrev();
                break;
            case R.id.txv_complete_inInputVeriCodeActivity:

                String str1 = getVerifyCode();
                String str2 = "123456";

                if (str1.equals(str2)){
                    gotoSuccessRegister();
                    gotoContactActivity();
                }else {

                    showAlertDialog("Wrong verification code. Please, insert again");
                    Initial();
                }

                break;
        }

    }



}
