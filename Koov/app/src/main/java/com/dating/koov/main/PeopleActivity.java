package com.dating.koov.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class PeopleActivity extends CommonActivity {

    LinearLayout ui_lyt_InstantVideo, ui_lyt_Message, ui_lyt_Contacts, ui_lyt_Me;

    ImageView ui_imv_Nearby;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people);

        loadLayout();
    }

    private void loadLayout() {
    }
}
