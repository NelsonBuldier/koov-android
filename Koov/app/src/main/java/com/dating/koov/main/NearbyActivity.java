package com.dating.koov.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dating.koov.R;
import com.dating.koov.adapter.NearbyAdapter;
import com.dating.koov.adapter.NearbyPagerAdapter;
import com.dating.koov.adapter.RecentAdapter;
import com.dating.koov.base.CommonActivity;

public class NearbyActivity extends CommonActivity implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener {

    LinearLayout ui_lyt_InstantVideo, ui_lyt_Message, ui_lyt_Contact, ui_lyt_Me;

    ImageView ui_imvIcon1, ui_imvIcon2, ui_imv_Message, ui_imv_Nearby;

    TextView  ui_txv_Nearby;

    ViewPager NearbyPager;
    TabLayout tabLayout;

    Integer _position = 1;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);

        NearbyPager = (ViewPager) findViewById(R.id.pager);
        tabLayout= (TabLayout) findViewById(R.id.tab_layout);

        loadLayout();


    }

    private void loadLayout() {

        ui_imv_Nearby = (ImageView)findViewById(R.id.imv_Nearby);
        ui_imv_Nearby.setImageResource(R.drawable.ic_nav_1_active);

        ui_txv_Nearby = (TextView)findViewById(R.id.txv_Nearby);
        ui_txv_Nearby.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Message = (LinearLayout)findViewById(R.id.lyt_Message);
        ui_lyt_Message.setOnClickListener(this);

        ui_lyt_Contact = (LinearLayout)findViewById(R.id.lyt_Contact);
        ui_lyt_Contact.setOnClickListener(this);

        ui_lyt_Me = (LinearLayout)findViewById(R.id.lyt_Me);
        ui_lyt_Me.setOnClickListener(this);

        ui_imvIcon1 = (ImageView) findViewById(R.id.imv1);
        ui_imvIcon1.setOnClickListener(this);

        ui_imvIcon2 = (ImageView) findViewById(R.id.imv2);
        ui_imvIcon2.setOnClickListener(this);

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        FragmentManager manager=getSupportFragmentManager();
        NearbyPagerAdapter adapter=new NearbyPagerAdapter(this, manager);
        NearbyPager.setAdapter(adapter);

        NearbyPager.setCurrentItem(1);

        tabLayout.setupWithViewPager(NearbyPager);
        NearbyPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position)
            {
                _position = position;
                setTab(position);
            }
        });

        tabLayout.setTabsFromPagerAdapter(adapter);


    }

    public void setTab(int tab) {

        switch (tab) {

            case 0:
                ui_imvIcon1.setImageBitmap(null);
                ui_imvIcon2.setImageResource(R.drawable.ic_chat_plus_press);
                break;

            case 1:
                ui_imvIcon1.setImageBitmap(null);
                ui_imvIcon2.setImageResource(R.drawable.ic_common_filter);
                break;

            case 2:
                ui_imvIcon1.setImageResource(R.drawable.ic_search);
                ui_imvIcon2.setImageResource(R.drawable.ic_add_new);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void gotoInstantVideoActivity() {

        Intent intent  = new Intent(this, InstantVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMessageActivity() {

        Intent intent = new Intent(this, MessageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoContactActivity() {

        Intent intent = new Intent(this, ContactActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMeActivity() {

        Intent intent = new Intent(this, MeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv1:
                if (_position == 2){
                    showAlertDialog("search");
                }
                break;
            case R.id.imv2:
                if (_position == 0){
                    showAlertDialog("circle_plus");
                }else if (_position ==1){
                    showAlertDialog("filter");
                }else if (_position == 2){
                    showAlertDialog("plus");
                }
                break;
            case R.id.lyt_InstantVideo:
                gotoInstantVideoActivity();
                break;
            case R.id.lyt_Message:
                gotoMessageActivity();
                break;
            case R.id.lyt_Contact:
                gotoContactActivity();
                break;
            case R.id.lyt_Me:
                gotoMeActivity();
                break;

        }

    }

    @Override
    public void onRefresh() {

    }

}
