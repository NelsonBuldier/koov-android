package com.dating.koov.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class MessageActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_Nearby, ui_lyt_InstantVideo, ui_lyt_Contact, ui_lyt_Me;

    ImageView ui_imv_Message;

    TextView ui_txv_Message;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_Message = (ImageView)findViewById(R.id.imv_Message);
        ui_imv_Message.setImageResource(R.drawable.ic_nav_3_active);

        ui_txv_Message = (TextView)findViewById(R.id.txv_Message);
        ui_txv_Message.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

        ui_lyt_Nearby = (LinearLayout)findViewById(R.id.lyt_Nearby);
        ui_lyt_Nearby.setOnClickListener(this);

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Contact = (LinearLayout)findViewById(R.id.lyt_Contact);
        ui_lyt_Contact.setOnClickListener(this);

        ui_lyt_Me = (LinearLayout)findViewById(R.id.lyt_Me);
        ui_lyt_Me.setOnClickListener(this);


    }


    private void gotoNearbyActivity() {

        Intent intent = new Intent(this, NearbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoInstantVideoActivity() {

        Intent intent = new Intent(this, InstantVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoContactActivity() {

        Intent intent = new Intent(this, ContactActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMeActivity() {

        Intent intent = new Intent(this, MeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lyt_Nearby:
                gotoNearbyActivity();
                break;
            case R.id.lyt_InstantVideo:
                gotoInstantVideoActivity();
                break;
            case R.id.lyt_Contact:
                gotoContactActivity();
                break;
            case R.id.lyt_Me:
                gotoMeActivity();
                break;
        }

    }

}
