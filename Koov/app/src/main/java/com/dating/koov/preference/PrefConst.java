package com.dating.koov.preference;

/**
 * Created by sts on 7/25/2016.
 */
public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_XMPPID = "XMPPID";
    public static final String PREFKEY_USERPWD = "password";

    public static final String PREFKEY_LASTLOGINID = "lastlogin_id";

    public static final String LOGIN_SUCCESS = "Login_Success";

}
