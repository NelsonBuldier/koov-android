package com.dating.koov.main;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;

public class
IntroActivity extends CommonActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        loadLayout();
    }

    private void loadLayout() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(IntroActivity.this, NearbyActivity.class);

                startActivity(intent);
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}

