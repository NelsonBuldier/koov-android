
package com.dating.koov.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class ForgotActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_back_inForgotActivity;
    LinearLayout ui_lyt_lock_inForgotActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_back_inForgotActivity = (ImageView)findViewById(R.id.imv_back_inForgotActivity);
        ui_imv_back_inForgotActivity.setOnClickListener(this);

        ui_lyt_lock_inForgotActivity = (LinearLayout)findViewById(R.id.lyt_lock_inForgotActivity);
        ui_lyt_lock_inForgotActivity.setOnClickListener(this);

    }

    private void gotoGetPasswordAcitivity() {

        Intent intent = new Intent(this, GetPasswordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back_inForgotActivity:

                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                break;

            case R.id.lyt_lock_inForgotActivity:

                gotoGetPasswordAcitivity();
        }

    }


}
