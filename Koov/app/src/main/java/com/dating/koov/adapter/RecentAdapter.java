package com.dating.koov.adapter;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.dating.koov.KoovApplication;
import com.dating.koov.R;
import com.dating.koov.details.UserUploadImagetActivity;
import com.dating.koov.fragments.RecentFragment;
import com.dating.koov.main.NearbyActivity;
import com.dating.koov.details.RecentUserDetailActivity;
import com.dating.koov.model.RecentUserEntitiy;
import com.dating.koov.model.UserEntity;

import java.util.ArrayList;

/**
 * Created by sts on 7/11/2016.
 */
public class RecentAdapter extends BaseAdapter implements View.OnClickListener {

    ArrayList<Object> _recent_user = new ArrayList<>();

    private NearbyActivity _context;

    ImageLoader _imageLoader;


    public RecentAdapter(NearbyActivity context, ArrayList<Object> recent_user){

        super();
        this._context = context;
        this._recent_user = recent_user;
        _imageLoader = KoovApplication.getInstance().getImageLoader();
    }

    @Override
    public int getCount() {return _recent_user.size();}

    @Override
    public Object getItem(int position) {  return _recent_user.get(position);    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView == null){

            holder = new CustomHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.recent_user_listview_item, parent, false);

            holder.imvUserPhoto = (NetworkImageView) convertView.findViewById(R.id.imv_photo_inRecent);
            holder.txvName = (TextView)convertView.findViewById(R.id.txv_name);

            holder.lytUserUploadImage = (LinearLayout)convertView.findViewById(R.id.lyt_user_upload_image);
            holder.lytUserUploadImage.setOnClickListener(this);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder) convertView.getTag();
        }

        final RecentUserEntitiy recent_user = (RecentUserEntitiy) _recent_user.get(position);

        holder.txvName.setText(recent_user.get_name());

        if (recent_user.get_photo_url().length() > 0)
            holder.imvUserPhoto.setImageUrl(recent_user.get_photo_url(), _imageLoader);
        else
            holder.imvUserPhoto.setImageResource(R.drawable.app_icon);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(_context, RecentUserDetailActivity.class);

                _context.startActivity(intent);
                _context.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        return convertView;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.lyt_user_upload_image:
                Intent intent = new Intent(_context, UserUploadImagetActivity.class);
                _context.startActivity(intent);
                _context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }

    }

    public class CustomHolder {

        public NetworkImageView imvUserPhoto ;
        public TextView txvName;
        public LinearLayout lytUserUploadImage;


    }

    public void setRecentUserDatas(ArrayList<Object> users ){

        _recent_user.clear();
        _recent_user.addAll(users);
    }

}
