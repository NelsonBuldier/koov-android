package com.dating.koov.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.adapter.VideoPagerAdapter;
import com.dating.koov.base.CommonActivity;

public class InstantVideoActivity extends CommonActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ImageView ui_imv_InstantVideo;
    LinearLayout ui_lyt_Nearby, ui_lyt_Message, ui_lyt_Contact, ui_lyt_Me;

    TextView ui_txv_Video;

    ViewPager ui_ViewPager_inVideoActivity;
    TabLayout ui_tabLayout_inVideoActivity;

    Integer _position = 1;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instant_video);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_InstantVideo = (ImageView)findViewById(R.id.imv_InstantVideo);
        ui_imv_InstantVideo.setImageResource(R.drawable.ic_nav_live_active);

        ui_lyt_Nearby = (LinearLayout)findViewById(R.id.lyt_Nearby);
        ui_lyt_Nearby.setOnClickListener(this);

        ui_lyt_Message = (LinearLayout)findViewById(R.id.lyt_Message);
        ui_lyt_Message.setOnClickListener(this);

        ui_lyt_Contact = (LinearLayout)findViewById(R.id.lyt_Contact);
        ui_lyt_Contact.setOnClickListener(this);

        ui_lyt_Me = (LinearLayout)findViewById(R.id.lyt_Me);
        ui_lyt_Me.setOnClickListener(this);

        ui_ViewPager_inVideoActivity = (ViewPager)findViewById(R.id.ViewPager_inVideoActivity);

        ui_tabLayout_inVideoActivity = (TabLayout)findViewById(R.id.tabLayout_inVideoActivity);

        ui_txv_Video = (TextView)findViewById(R.id.txv_Video);
        ui_txv_Video.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

//        ui_tabLayout_inVideoActivity.setOnClickListener(this);



        FragmentManager manager = getSupportFragmentManager();
        VideoPagerAdapter videoPagerAdapter = new VideoPagerAdapter(this, manager);
        ui_ViewPager_inVideoActivity.setAdapter(videoPagerAdapter);

        ui_tabLayout_inVideoActivity.setupWithViewPager(ui_ViewPager_inVideoActivity);

        ui_tabLayout_inVideoActivity.setTabsFromPagerAdapter(videoPagerAdapter);


    }

    private void gotoNearbyActivity() {

        Intent intent = new Intent(this, NearbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();

    }

    private void gotoMessageActivity() {

        Intent intent = new Intent(this, MessageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();

    }

    private void gotoContactActivity() {

        Intent intent = new Intent(this, ContactActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMeActivity() {

        Intent intent = new Intent(this, MeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.tabLayout_inVideoActivity:
                showAlertDialog("Ok");
                break;

            case R.id.lyt_Nearby:
                gotoNearbyActivity();
                break;
            case R.id.lyt_Message:
                gotoMessageActivity();
                break;
            case R.id.lyt_Contact:
                gotoContactActivity();
                break;
            case R.id.lyt_Me:
                gotoMeActivity();
                break;
        }

    }

    @Override
    public void onRefresh() {

    }
}

