package com.dating.koov.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.main.NearbyActivity;

import java.util.ArrayList;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by sts on 7/14/2016.
 */
public class GroupAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private NearbyActivity _context;
    private LayoutInflater mInflater;
    private String[] mCountries;
    private int[] mSectionIndices;
    private Character[] mSectionLetters;

    public GroupAdapter(NearbyActivity context) {

        super();
        this._context = context;
        mInflater = LayoutInflater.from(context);
        mCountries = context.getResources().getStringArray(R.array.countries);

        mSectionIndices = getSectionIndices();
        mSectionLetters = getSectionLetters();
    }

    private int[] getSectionIndices() {
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
        char lastFirstChar = mCountries[0].charAt(0);
        sectionIndices.add(0);
        for (int i = 1; i < mCountries.length; i++) {
            if (mCountries[i].charAt(0) != lastFirstChar) {
                lastFirstChar = mCountries[i].charAt(0);
                sectionIndices.add(i);
            }
        }
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    private Character[] getSectionLetters() {
        Character[] letters = new Character[mSectionIndices.length];
        for (int i = 0; i < mSectionIndices.length; i++) {
            letters[i] = mCountries[mSectionIndices[i]].charAt(0);
        }
        return letters;
    }



    @Override
    public int getCount() {
        return mCountries.length;
    }

    @Override
    public Object getItem(int position) {        return mCountries[position];    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null){
            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.group_user_listview_item, parent, false);
            holder.img_photo = (ImageView)convertView.findViewById(R.id.imv_photo_inGroup);


            holder.text = (TextView)convertView.findViewById(R.id.txv_name_inGroup);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.text.setText(mCountries[position]);
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        HeaderViewHolder holder;

        if (convertView == null){
            holder = new HeaderViewHolder();

            convertView = mInflater.inflate(R.layout.group_header, parent, false);
            holder.text = (TextView)convertView.findViewById(R.id.txv_group_header);
            convertView.setTag(holder);
        }else {
            holder = (HeaderViewHolder)convertView.getTag();
        }

        CharSequence headerChar = mCountries[position].subSequence(0, 1);
        holder.text.setText(headerChar);

        return convertView;
    }

    @Override
    public long getHeaderId(int position) {

        return mCountries[position].subSequence(0, 1).charAt(0);
    }

    class HeaderViewHolder {
        TextView text;
    }

    class ViewHolder {
        ImageView img_photo;
        TextView text;
    }
}
