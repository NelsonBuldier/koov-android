package com.dating.koov.model;


import java.io.Serializable;

/**
 * Created by JIS on 2/10/2016.
 */
public class UserEntity implements Serializable {

    String name = "";
    String phone = "";
    String mail = "";

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMail() {

        return mail;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
