package com.dating.koov.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.dating.koov.R;
import com.dating.koov.details.UserUploadImagetActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/25/2016.
 */
public class ViewPagerAdapter extends PagerAdapter{

    ArrayList<Integer > upload_images = new ArrayList<>();

    Context mContext ;
    LayoutInflater mLayoutInflater;

    public ViewPagerAdapter(Context context, ArrayList<Integer> imgs) {

        this.mContext = context;
        mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.upload_images = imgs;
    }

    @Override
    public int getCount() {
        return upload_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = mLayoutInflater.inflate(R.layout.upload_images, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imv_upload_image);
        imageView.setImageResource(upload_images.get(position));
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
