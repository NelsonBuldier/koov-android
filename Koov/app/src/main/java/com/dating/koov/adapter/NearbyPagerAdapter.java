package com.dating.koov.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.dating.koov.fragments.PeopleFragment;
import com.dating.koov.fragments.RecentFragment;
import com.dating.koov.fragments.GroupFragment;
import com.dating.koov.main.NearbyActivity;

/**
 * Created by kundan on 10/16/2015.
 */
public class NearbyPagerAdapter extends FragmentStatePagerAdapter {

    private NearbyActivity _nearbyActivity;

    public NearbyPagerAdapter(NearbyActivity nearbyActivity, FragmentManager fm ) {

        super(fm);
        this._nearbyActivity = nearbyActivity;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment frag=null;
        switch (position){
            case 0:
                frag=new RecentFragment(_nearbyActivity);
                break;
            case 1:
                frag = new PeopleFragment(_nearbyActivity);
                break;
            case 2:
                frag=new GroupFragment(_nearbyActivity);
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title="Recent";
                break;
            case 1:
                title="People";
                break;
            case 2:
                title="Group";
                break;
        }

        return title;
    }

}
