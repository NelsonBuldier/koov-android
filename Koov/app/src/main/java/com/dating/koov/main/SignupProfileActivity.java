package com.dating.koov.main;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;
import com.dating.koov.model.UserEntity;
import com.dating.koov.utils.BitmapUtils;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.io.InputStream;


//@SuppressWarnings("deprecation")
public class SignupProfileActivity extends CommonActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener{

    LinearLayout ui_lyt_female_inLoginSecondActivity, ui_lyt_male_inLoginSecondActivity ;

    LinearLayout ui_lyt_back_inLoginProfileActivity;

    FrameLayout ui_frm_photo_inLoginSecondActivity;

    TextView ui_txv_name_inLoginSecondActivity;
    TextView ui_txv_addPhoto_inLoginSecondActivity, ui_txv_next_inLoginSecondActivity, ui_txv_native_select_inLoginSecondActivity;

    ImageView ui_imv_camera_inLoginSecondActivity;

    EditText ui_edt_birthday_inLoginSecondActivity;

    ImageView ui_imv_male_inLoginSecondActivity, ui_imv_female_inLoginSecondActivity;
    ImageView ui_imvRegPhoto;

    UserEntity _thisUser = null;


    private Uri _imageCaptureUri;

    Animation hyper_show, hyper_hide;

    String _photoPath = "";

    Boolean diplay_alert_flag = true;

    Boolean photo_flag = false, birthday_flag = false, sex_flag = false, city_flag = false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_profile);

        _thisUser = (UserEntity)getIntent().getSerializableExtra(Constants.USER);

        loadLayout();

    }

    private void loadLayout() {

        ui_imvRegPhoto = (RoundedImageView) findViewById(R.id.imv_addphoto);

        ui_imv_camera_inLoginSecondActivity = (ImageView)findViewById(R.id.imv_camera_inLoginSecondActivity);

        ui_txv_addPhoto_inLoginSecondActivity = (TextView)findViewById(R.id.txv_addPhoto_inLoginSecondActivity);

        ui_txv_next_inLoginSecondActivity = (TextView)findViewById(R.id.txv_next_inLoginSecondActivity);

        ui_lyt_male_inLoginSecondActivity = (LinearLayout)findViewById(R.id.lyt_male_inLoginSecondActivity);
        ui_lyt_male_inLoginSecondActivity.setOnClickListener(this);


        ui_lyt_female_inLoginSecondActivity = (LinearLayout)findViewById(R.id.lyt_female_inLoginSecondActivity);
        ui_lyt_female_inLoginSecondActivity.setOnClickListener(this);

        ui_lyt_back_inLoginProfileActivity = (LinearLayout) findViewById(R.id.lyt_back_inLoginProfileActivity);
        ui_lyt_back_inLoginProfileActivity.setOnClickListener(this);

        ui_imv_male_inLoginSecondActivity = (ImageView)findViewById(R.id.imv_male_inLoginSecondActivity);

        ui_imv_female_inLoginSecondActivity = (ImageView)findViewById(R.id.imv_female_inLoginSecondActivity);

        ui_txv_name_inLoginSecondActivity = (TextView) findViewById(R.id.txv_name_inLoginSecondActivity);
        ui_txv_name_inLoginSecondActivity.setText(_thisUser.getName());

        ui_edt_birthday_inLoginSecondActivity = (EditText)findViewById(R.id.edt_birthday_inLoginSecondActivity);
        ui_edt_birthday_inLoginSecondActivity.setOnClickListener(this);
        ui_edt_birthday_inLoginSecondActivity.setInputType(InputType.TYPE_NULL);

        ui_txv_native_select_inLoginSecondActivity = (TextView) findViewById(R.id.txv_native_select_inLoginSecondActivity);
        ui_txv_native_select_inLoginSecondActivity.setOnClickListener(this);
        ui_txv_native_select_inLoginSecondActivity.setInputType(InputType.TYPE_NULL);

        ui_frm_photo_inLoginSecondActivity = (FrameLayout) findViewById(R.id.frm_photo_inLoginSecondActivity);
        ui_frm_photo_inLoginSecondActivity.setOnClickListener(this);

        hyper_show = AnimationUtils.loadAnimation(this, R.anim.anim_btn_publish_feed_show);
        hyper_hide = AnimationUtils.loadAnimation(this,R.anim.anim_btn_publish_feed_hide);


    }

    private void showDatePicker() {

        showDialog(Constants.DATE_DIALOG);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == Constants.DATE_DIALOG){

            java.util.Calendar now = java.util.Calendar.getInstance();
            int year = now.get(java.util.Calendar.YEAR);
            int month = now.get(java.util.Calendar.MONTH);
            int day = now.get(java.util.Calendar.DAY_OF_MONTH);

            return new android.app.DatePickerDialog(this, dateListener, year, month, day);
        }
        return super.onCreateDialog(id);
    }

    private android.app.DatePickerDialog.OnDateSetListener dateListener = new android.app.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {

            String date = String.format("%d-%02d-%02d", year, month + 1, day);

            ui_edt_birthday_inLoginSecondActivity.setText(date);

            birthday_flag = true;
            if (photo_flag && birthday_flag && sex_flag ) {

                ui_txv_next_inLoginSecondActivity.setOnClickListener(SignupProfileActivity.this);
                ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xff,0xff,0xff));
            }else{

                ui_txv_next_inLoginSecondActivity.setOnClickListener(null);
                ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
            }
        }
    };

    private void getUserPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else {
                    doTakeGallery();
                }
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

        photo_flag = true;

        if (photo_flag && birthday_flag && sex_flag && city_flag) {

            ui_txv_next_inLoginSecondActivity.setOnClickListener(SignupProfileActivity.this);
            ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
            ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xff,0xff,0xff));
        }else{

            ui_txv_next_inLoginSecondActivity.setOnClickListener(null);
            ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
            ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
        }

    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String picturePath = BitmapUtils.getTempFolderPath() + "photo_temp.jpg";
        _imageCaptureUri = Uri.fromFile(new File(picturePath));

        intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
        startActivityForResult(intent, Constants.PICK_FROM_CAMERA);

        ui_imv_camera_inLoginSecondActivity.setVisibility(View.GONE);
        ui_txv_addPhoto_inLoginSecondActivity.setVisibility(View.GONE);

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);

        ui_imv_camera_inLoginSecondActivity.setVisibility(View.GONE);
        ui_txv_addPhoto_inLoginSecondActivity.setVisibility(View.GONE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        ui_imvRegPhoto.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void showCityPicker() {

        final String[] values = getResources().getStringArray(R.array.countries);

        final String[] _city = {""};

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        LayoutInflater layout = LayoutInflater.from(this);
        View promptsView = layout.inflate(R.layout.city_picker, null);

        alert.setView(promptsView);

        NumberPicker ui_Npic_city_picker =(NumberPicker) promptsView.findViewById(R.id.Npic_city_picker);

        ui_Npic_city_picker.setMinValue(0);
        ui_Npic_city_picker.setMaxValue(values.length-1);

        ui_Npic_city_picker.setDisplayedValues(values);

        ui_Npic_city_picker.setWrapSelectorWheel(true);

        ui_Npic_city_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int oldVal, int newVal) {

                _city[0] = values[newVal];
            }
        });

        alert.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                ui_txv_native_select_inLoginSecondActivity.setText(_city[0]);
                dialogInterface.cancel();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.cancel();
            }
        });

        city_flag = true;




        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void gotoSignupPhoneNumberActivity() {

        if (diplay_alert_flag) {

            android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);

            alertDialog.setTitle("Warring");
            alertDialog.setMessage("You can't change gender after registration.");

            alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {

                    Intent intent = new Intent(SignupProfileActivity.this, SignupPhoneNumberActivity.class);
                    startActivity(intent);
                    SignupProfileActivity.this.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                    SignupProfileActivity.this.diplay_alert_flag = false;

                    dialogInterface.cancel();
                }
            });

            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    dialogInterface.cancel();
                }
            });

            alertDialog.show();

        }else {

            Intent intent = new Intent(SignupProfileActivity.this, SignupPhoneNumberActivity.class);
            startActivity(intent);
            SignupProfileActivity.this.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.lyt_back_inLoginProfileActivity:

                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

                break;

            case R.id.lyt_male_inLoginSecondActivity:

                ui_imv_male_inLoginSecondActivity.setImageResource(R.drawable.bg_radiobutton_press);
                ui_imv_male_inLoginSecondActivity.startAnimation(hyper_show);

                ui_imv_female_inLoginSecondActivity.setImageResource(R.drawable.ic_like_match_normal);
                ui_imv_female_inLoginSecondActivity.startAnimation(hyper_hide);

                sex_flag = true;
                if (photo_flag && birthday_flag && sex_flag && city_flag ) {

                    ui_txv_next_inLoginSecondActivity.setOnClickListener(SignupProfileActivity.this);
                    ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xff,0xff,0xff));
                }else{

                    ui_txv_next_inLoginSecondActivity.setOnClickListener(null);
                    ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                }
                break;

            case R.id.lyt_female_inLoginSecondActivity:

                ui_imv_male_inLoginSecondActivity.setImageResource(R.drawable.ic_like_match_normal);
                ui_imv_male_inLoginSecondActivity.startAnimation(hyper_hide);

                ui_imv_female_inLoginSecondActivity.setImageResource(R.drawable.bg_radiobutton_press);
                ui_imv_female_inLoginSecondActivity.startAnimation(hyper_show);

                sex_flag = true;
                if (photo_flag && birthday_flag && sex_flag && city_flag) {

                    ui_txv_next_inLoginSecondActivity.setOnClickListener(SignupProfileActivity.this);
                    ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xff,0xff,0xff));
                }else{

                    ui_txv_next_inLoginSecondActivity.setOnClickListener(null);
                    ui_txv_next_inLoginSecondActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_next_inLoginSecondActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                }
                break;

            case R.id.edt_birthday_inLoginSecondActivity:

                showDatePicker();
                break;

            case R.id.frm_photo_inLoginSecondActivity:
                getUserPhoto();
                break;

            case R.id.txv_native_select_inLoginSecondActivity:
                showCityPicker();
                break;
            case R.id.txv_next_inLoginSecondActivity:
                gotoSignupPhoneNumberActivity();
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {}
}
