package com.dating.koov.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.dating.koov.R;
import com.dating.koov.adapter.SuggestAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.InstantVideoActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/2016.
 */
public class SuggestFragment extends BaseFragment {

    private InstantVideoActivity instantVideoActivity;

    private SwipeRefreshLayout ui_Swipe_Refresh_inSuggestFragment;
    private GridView ui_GridView_inFragmentSuggest;

    private SuggestAdapter suggestAdapter;

    private ArrayList<MyItem> arItem;

    public SuggestFragment(InstantVideoActivity context) {

        this.instantVideoActivity = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_suggest, container, false);

        ui_GridView_inFragmentSuggest = (GridView)view.findViewById(R.id.GridView_inFragmentSuggest);

        ui_Swipe_Refresh_inSuggestFragment = (SwipeRefreshLayout) view.findViewById(R.id.Swipe_Refresh_inSuggestFragment);
        ui_Swipe_Refresh_inSuggestFragment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ui_Swipe_Refresh_inSuggestFragment.setRefreshing(false);
                    }
                }, 1000);
            }
        });



        arItem = new ArrayList<>();

        MyItem mi;
        mi = new MyItem(R.drawable.ic_welcome_photo_1, "Jacson"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_2, "Sims"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_3, "Jerry"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_4, "Star"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_5, "aaa"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_6, "bbb"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_7, "ccc"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_8, "ddd"); arItem.add(mi);


        suggestAdapter = new SuggestAdapter(instantVideoActivity, arItem);
        ui_GridView_inFragmentSuggest.setAdapter(suggestAdapter);


        return view;
     }

    public class MyItem {

        public int Icon;
        public String Name;

        MyItem(int aIcon, String aName){
            Icon = aIcon;
            Name = aName;
        }
    }
}
