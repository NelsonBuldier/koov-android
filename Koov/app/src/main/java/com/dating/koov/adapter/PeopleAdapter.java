package com.dating.koov.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.fragments.PeopleFragment;
import com.dating.koov.main.NearbyActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/13/2016.
 */
public class PeopleAdapter extends BaseAdapter {

    private NearbyActivity _context;

    private ArrayList<PeopleFragment.MyItem> _users = new ArrayList<>();

    public PeopleAdapter(NearbyActivity context, ArrayList<PeopleFragment.MyItem> users){

        super();
        this._context = context;
        this._users = users;
    }
    @Override
    public int getCount() {        return _users.size();    }

    @Override
    public Object getItem(int position) {        return _users.get(position).Name;    }

    @Override
    public long getItemId(int position) {        return position;    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        CustomHolder holder;
        if (convertView == null){

            holder = new CustomHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.people_user_listview_item, parent, false );

            holder.imvPhoto = (ImageView)convertView.findViewById(R.id.imv_photo_inPeople);
            holder.imvPhoto.setImageResource(_users.get(position).Icon);

            holder.txvName = (TextView)convertView.findViewById(R.id.txv_name_inPeople);
            holder.txvName.setText(_users.get(position).Name);

            convertView.setTag(holder);
        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _context.showToast(String.valueOf(position));

            }
        });

        return convertView;
    }

    public class CustomHolder {

        public ImageView imvPhoto;
        public TextView txvName;
        public TextView txvMajor;
    }
}
