package com.dating.koov.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;
import com.dating.koov.model.UserEntity;

import java.util.Arrays;

public class SignupPhoneNumberActivity extends CommonActivity implements View.OnClickListener {


    LinearLayout ui_lyt_back_inInputPhoneNumberActivity;

    EditText ui_edt_phone_inInputPhoneNumberActivity;
    EditText ui_edt_password_inInputPhoneNumberActivity;
    TextView ui_txv_postal_number_inInputPhoneNumberActivity, ui_txv_next_inInputPhoneNumberActivity;

    UserEntity _user = new UserEntity();

    String postal_number = "", password = "", phone_number ="";
    String phone_number_style = "111-1111-1111";

    int cnt = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_phone_number);

        loadLayout();
    }

    private void loadLayout() {

        ui_lyt_back_inInputPhoneNumberActivity = (LinearLayout) findViewById(R.id.lyt_back_inInputPhoneNumberActivity);
        ui_lyt_back_inInputPhoneNumberActivity.setOnClickListener(this);

        ui_txv_next_inInputPhoneNumberActivity = (TextView)findViewById(R.id.txv_next_inInputPhoneNumberActivity);

        ui_txv_postal_number_inInputPhoneNumberActivity = (TextView) findViewById(R.id.txv_postal_number_inInputPhoneNumberActivity);
        ui_txv_postal_number_inInputPhoneNumberActivity.setOnClickListener(this);

        ui_edt_password_inInputPhoneNumberActivity = (EditText)findViewById(R.id.edt_password_inInputPhoneNumberActivity);
        ui_edt_password_inInputPhoneNumberActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                password = ui_edt_password_inInputPhoneNumberActivity.getText().toString().trim();

                if (password.length() == 0 || phone_number.length() == 0 || postal_number.length() == 0){

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(null);

                }else {

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(SignupPhoneNumberActivity.this);
                }

            }
        });

        ui_edt_phone_inInputPhoneNumberActivity = (EditText) findViewById(R.id.edt_phone_inInputPhoneNumberActivity);
        ui_edt_phone_inInputPhoneNumberActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int before) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                phone_number = ui_edt_phone_inInputPhoneNumberActivity.getText().toString();

                if (password.length() == 0 || phone_number.length() == 0 || postal_number.length() == 0){

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(null);

                }else {

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(SignupPhoneNumberActivity.this);
                }

                ui_edt_phone_inInputPhoneNumberActivity.removeTextChangedListener(this);

                phone_number = ui_edt_phone_inInputPhoneNumberActivity.getText().toString();

                for (int i = 0; i < phone_number.length(); i++){

                    if (phone_number_style.charAt(i) == '-' && phone_number.charAt(i) !='-'){
                        phone_number = new StringBuffer(phone_number).insert(i, "-").toString();
                    }
                }


                ui_edt_phone_inInputPhoneNumberActivity.setText(phone_number);

                ui_edt_phone_inInputPhoneNumberActivity.addTextChangedListener(this);

                ui_edt_phone_inInputPhoneNumberActivity.setSelection(phone_number.length());

            }


        });

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_phone_inInputPhoneNumberActivity.getWindowToken(), 0);
                return false;
            }
        });

    }

    private void showPostalNumber() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Select postal number");
        final String[] postal_number_array = getResources().getStringArray(R.array.postal_number);

        adb.setItems(postal_number_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                String clickedItemValue = Arrays.asList(postal_number_array).get(which);
                StringBuilder postal = new StringBuilder();

                for (int i= 0; i<clickedItemValue.length(); i++){

                    char c= clickedItemValue.charAt(i);
                    if (Character.isDigit(c)){
                        postal.append(c);
                    }
                }

                ui_txv_postal_number_inInputPhoneNumberActivity.setText(Arrays.asList(postal_number_array).get(which));
                postal_number ="(+" + postal.toString() + ")";

                if (phone_number.length() == 0 || password.length() == 0 || postal_number.length() == 0){

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(null);

                }else {

                    ui_txv_next_inInputPhoneNumberActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_next_inInputPhoneNumberActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_next_inInputPhoneNumberActivity.setOnClickListener(SignupPhoneNumberActivity.this);
                }
            }

        });

        adb.show().getWindow().setLayout(650, 1000);

    }

    private boolean checked_password() {

        if (password.length() < 6){
            showAlertDialog("Password must be longer than 6 characters.");
            return false;
        }
        return true;
    }

    private void gotoSignupVerifyCodeActivity(CharSequence msg) {

        _user.setPhone(postal_number + msg);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Confirm the first phone number.");
        alertDialog.setMessage(msg);

        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                Intent intent = new Intent(SignupPhoneNumberActivity.this, SignupVerifyCodeActivity.class);
                intent.putExtra(Constants.USER, _user);

                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                dialogInterface.cancel();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.cancel();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lyt_back_inInputPhoneNumberActivity:

                finish();
                overridePendingTransition(R.anim.push_out_right , R.anim.pull_in_left);
                break;
            case R.id.txv_postal_number_inInputPhoneNumberActivity:

                showPostalNumber();
                break;
            case R.id.txv_next_inInputPhoneNumberActivity:

                if(checked_password()){
                    gotoSignupVerifyCodeActivity(phone_number);
                }
                break;
        }

    }




}


