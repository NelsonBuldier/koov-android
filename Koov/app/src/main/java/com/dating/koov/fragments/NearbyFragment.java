package com.dating.koov.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.dating.koov.R;
import com.dating.koov.adapter.NearbyAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.InstantVideoActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/2016.
 */
public class NearbyFragment extends BaseFragment{

    InstantVideoActivity instantVideoActivity;

    private SwipeRefreshLayout ui_Swipe_Refresh_inNearByFragment;

    private NearbyAdapter nearbyAdapter;

    private GridView ui_gridView_inNearbyFragment;

    private ArrayList<MyItem> arItem;

    public NearbyFragment (InstantVideoActivity context){

        this.instantVideoActivity = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nearby, container, false);


        ui_gridView_inNearbyFragment = (GridView)view.findViewById(R.id.gridView_inNearbyFragment);

        ui_Swipe_Refresh_inNearByFragment = (SwipeRefreshLayout) view.findViewById(R.id.Swipe_Refresh_inNearbyFragment);
        ui_Swipe_Refresh_inNearByFragment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ui_Swipe_Refresh_inNearByFragment.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        arItem = new ArrayList<>();

        MyItem mi;
        mi = new MyItem(R.drawable.ic_welcome_photo_10, "Jacson"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_9, "Sims"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_7, "Jerry"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_12, "Star"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_15, "aaa"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_16, "bbb"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_5, "ccc"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_2, "ddd"); arItem.add(mi);

        nearbyAdapter = new NearbyAdapter(instantVideoActivity, arItem);
        ui_gridView_inNearbyFragment.setAdapter(nearbyAdapter);

        return view;
    }

    public class MyItem {

        public int Icon;
        public String Name;

        MyItem(int aIcon, String aName){
            Icon = aIcon;
            Name = aName;
        }
    }
}
