package com.dating.koov.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.adapter.RecentAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.NearbyActivity;
import com.dating.koov.model.RecentUserEntitiy;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecentFragment extends BaseFragment implements View.OnClickListener{

    private View alertDialog;

    private NearbyActivity nearbyActivity;
    private ListView ui_listRecent;
    private RecentAdapter _adapter;
    private SwipeRefreshLayout ui_Swipe_Refresh_inRecent;

    RecentUserEntitiy recent_user = new RecentUserEntitiy();

    ArrayList<Object> _recent_user = new ArrayList<>();

    public RecentFragment(NearbyActivity context) {

        this.nearbyActivity = context;
        // Required empty public constructor
    }

    private void downloadData() {

        for (int i = 0; i<5; i++){

            String name = ("name" + String.valueOf(i)).toString().trim();
            recent_user.set_name(name);

            String photo_url =("R.drawable.ic_welcome_photo_" + String.valueOf(i)).toString().trim();
            recent_user.set_photo_url(photo_url);

            ArrayList<String> user_upload_img = new ArrayList<>();
            for (int j = 0; j<3; j++){

                String img_url = ("R.drawable.th" + String.valueOf(i)).toString().trim();
                user_upload_img.add(img_url);
            }

            recent_user.set_upload_img_url(user_upload_img);

            _recent_user.add(recent_user);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recent, container , false);

        ui_Swipe_Refresh_inRecent = (SwipeRefreshLayout)view.findViewById(R.id.Swipe_Refresh_inRecent);

        ui_Swipe_Refresh_inRecent.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ui_Swipe_Refresh_inRecent.setRefreshing(false);
                    }
                },1000);
            }
        });

        downloadData();

        _adapter = new RecentAdapter(nearbyActivity, _recent_user);

        ui_listRecent = (ListView) view.findViewById(R.id.lst_users_inRecent);

        ui_listRecent.setAdapter(_adapter);


        TextView ui_txv_select_inRecentFragment = (TextView)view.findViewById(R.id.txv_select_inRecentFragment);
        ui_txv_select_inRecentFragment.setOnClickListener(this);

        alertDialog = inflater.inflate(R.layout.custom_alert_dialog_in_recent_fragment, container, false);

        return view;

    }


    private void showAlertDialog() {

        LayoutInflater layout = LayoutInflater.from(nearbyActivity);
        View promptsView = layout.inflate(R.layout.custom_alert_dialog_in_recent_fragment, null);

        final TextView ui_txv_user_sex_other =(TextView) promptsView.findViewById(R.id.txv_user_sex_other);
        ui_txv_user_sex_other.setBackgroundResource(R.drawable.bg_txv__btn_custom_alert_dialog);

        final LinearLayout ui_lyt_user_sex_female = (LinearLayout)promptsView.findViewById(R.id.lyt_user_sex_female);
        ui_lyt_user_sex_female.setBackground(null);

        final LinearLayout ui_lyt_user_sex_male = (LinearLayout)promptsView.findViewById(R.id.lyt_user_sex_male);
        ui_lyt_user_sex_male.setBackground(null);

        final com.dating.koov.views.ToggleButton ui_togbtn_display_distance = (com.dating.koov.views.ToggleButton) promptsView.findViewById(R.id.togbtn_display_distance);
        //ui_togbtn_display_distance.isInputMethodTarget();

        AlertDialog.Builder alert = new AlertDialog.Builder(nearbyActivity);

        alert.setView(promptsView);

        ui_txv_user_sex_other.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ui_txv_user_sex_other.setBackgroundResource(R.drawable.bg_txv__btn_custom_alert_dialog);
                ui_lyt_user_sex_female.setBackground(null);
                ui_lyt_user_sex_male.setBackground(null);
            }
        });

        ui_lyt_user_sex_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ui_txv_user_sex_other.setBackground(null);
                ui_lyt_user_sex_female.setBackgroundResource(R.drawable.bg_txv__btn_custom_alert_dialog);
                ui_lyt_user_sex_male.setBackground(null);
            }
        });

        ui_lyt_user_sex_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ui_txv_user_sex_other.setBackground(null);
                ui_lyt_user_sex_female.setBackground(null);
                ui_lyt_user_sex_male.setBackgroundResource(R.drawable.bg_txv__btn_custom_alert_dialog);
            }
        });


        AlertDialog dialog = alert.create();
        dialog.show();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.txv_select_inRecentFragment:
                showAlertDialog();
                break;

        }

    }

}

