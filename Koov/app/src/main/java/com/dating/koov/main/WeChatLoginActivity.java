package com.dating.koov.main;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class WeChatLoginActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_back_inWeChatActivity, ui_imv_AccountClear_inWeChatActivity, ui_imv_PwdClear_inWeChatActivity;

    EditText ui_edt_account_inWeChatActivity, ui_edt_pwd_inWeChatActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_we_chat_login);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_back_inWeChatActivity = (ImageView)findViewById(R.id.imv_back_inWeChatActivity);
        ui_imv_back_inWeChatActivity.setOnClickListener(this);

        ui_imv_AccountClear_inWeChatActivity = (ImageView)findViewById(R.id.imv_AccountClear_inWeChatActivity);

        ui_imv_PwdClear_inWeChatActivity = (ImageView)findViewById(R.id.imv_PwdClear_inWeChatActivity);

        ui_edt_account_inWeChatActivity = (EditText)findViewById(R.id.edt_Account_inWeChatActivity);
        ui_edt_account_inWeChatActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                String text = ui_edt_account_inWeChatActivity.getText().toString().trim();

                if (text.length() == 0 ){

                    ui_imv_AccountClear_inWeChatActivity.setVisibility(View.INVISIBLE);
                    ui_imv_AccountClear_inWeChatActivity.setOnClickListener(null);
                }else {

                    ui_imv_AccountClear_inWeChatActivity.setVisibility(View.VISIBLE);
                    ui_imv_AccountClear_inWeChatActivity.setOnClickListener(WeChatLoginActivity.this);
                }
            }
        });

        ui_edt_pwd_inWeChatActivity = (EditText)findViewById(R.id.edt_pwd_inWeChatActivity);
        ui_edt_pwd_inWeChatActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                String text = ui_edt_pwd_inWeChatActivity.getText().toString().trim();

                if (text.length() == 0){

                    ui_imv_PwdClear_inWeChatActivity.setVisibility(View.INVISIBLE);
                    ui_imv_PwdClear_inWeChatActivity.setOnClickListener(null);
                }else {

                    ui_imv_PwdClear_inWeChatActivity.setVisibility(View.VISIBLE);
                    ui_imv_PwdClear_inWeChatActivity.setOnClickListener(WeChatLoginActivity.this);
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.imv_back_inWeChatActivity:

                finish();
                break;

            case R.id.imv_AccountClear_inWeChatActivity:

                ui_edt_account_inWeChatActivity.setText("");
                break;

            case R.id.imv_PwdClear_inWeChatActivity:

                ui_edt_pwd_inWeChatActivity.setText("");
                break;
        }
    }

}
