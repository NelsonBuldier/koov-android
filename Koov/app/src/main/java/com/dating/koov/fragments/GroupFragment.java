package com.dating.koov.fragments;


import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.dating.koov.R;
import com.dating.koov.adapter.GroupAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.NearbyActivity;

import java.util.ArrayList;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class GroupFragment extends BaseFragment implements
    AdapterView.OnItemClickListener,
    StickyListHeadersListView.OnHeaderClickListener{

    private NearbyActivity nearbyActivity;
    private StickyListHeadersListView stickList;
    private DrawerLayout mDrawerLayout;

    private GroupAdapter mAdapter;

    private ActionBarDrawerToggle mDrawerToggle;

    private List<Long> mCollapseHeaderIds = new ArrayList<Long>();

    public GroupFragment(NearbyActivity context) {

        this.nearbyActivity = context;
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_group, container, false);

        mAdapter = new GroupAdapter(nearbyActivity);

        stickList = (StickyListHeadersListView)view.findViewById(R.id.list_inGroup);
        stickList.setOnClickListener(nearbyActivity);


        stickList.setDrawingListUnderStickyHeader(true);
        stickList.setAreHeadersSticky(true);
        stickList.setAdapter(mAdapter);

        stickList.setOnHeaderClickListener(this);
        stickList.setOnItemClickListener(this);

        stickList.setAdapter(mAdapter);

        return view;
    }


    @Override
    public void onHeaderClick(StickyListHeadersListView lst, View header, int itemPosition, long headerId, boolean currentlySticky) {

        if (mCollapseHeaderIds.contains(headerId)){
            Toast.makeText(nearbyActivity, String.valueOf(headerId ), Toast.LENGTH_SHORT).show();
        }
        Toast.makeText(nearbyActivity, "Ok", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

        Toast.makeText(nearbyActivity, "Item " + position + " clicked!", Toast.LENGTH_SHORT).show();
    }


}

