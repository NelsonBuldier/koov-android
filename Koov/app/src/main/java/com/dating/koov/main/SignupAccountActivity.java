package com.dating.koov.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;
import com.dating.koov.model.UserEntity;

public class SignupAccountActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_back_inLoginActivity;
    ImageView ui_we_chat;

    TextView ui_txv_explain_inLoginActivity, ui_txv_next_inLoginActivity;

    EditText ui_edt_nickname_inLoginActivity;

    UserEntity _user = new UserEntity();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_account);

        loadLayout();
    }

    private void loadLayout() {

        ui_lyt_back_inLoginActivity = (LinearLayout)findViewById(R.id.lyt_back_inLoginActivity);
        ui_lyt_back_inLoginActivity.setOnClickListener(this);

        ui_txv_explain_inLoginActivity = (TextView)findViewById(R.id.txv_explain_inLoginActivity);
        ui_txv_explain_inLoginActivity.setOnClickListener(this);

        ui_txv_next_inLoginActivity = (TextView)findViewById(R.id.txv_next_inLoginActivity);

        ui_we_chat = (ImageView)findViewById(R.id.imv_we_chat);
        ui_we_chat.setOnClickListener(this);

        ui_edt_nickname_inLoginActivity = (EditText)findViewById(R.id.edt_nickname_inLoginActivity);
        ui_edt_nickname_inLoginActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count ) {}

            @Override
            public void afterTextChanged(Editable editable) {

                String text = ui_edt_nickname_inLoginActivity.getText().toString().trim();
                if (text.length() == 0){

                    ui_txv_next_inLoginActivity.setOnClickListener(null);

                    ui_txv_next_inLoginActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_next_inLoginActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);

                }else {

                    ui_txv_next_inLoginActivity.setOnClickListener(SignupAccountActivity.this);

                    ui_txv_next_inLoginActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_next_inLoginActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);

                }
            }
        });

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_nickname_inLoginActivity.getWindowToken(), 0);
                return false;
            }
        });

    }

    private void gotoBack() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Do you finish?");

        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                finish();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

            }
        });

        alertDialog.show();
    }

    private void showExplain() {

        final AlertDialog.Builder customBuilder = new AlertDialog.Builder(this);

        customBuilder.setView(R.layout.explain_user_login);

        customBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.cancel();
            }
        });

        AlertDialog dialog = customBuilder.create();

        dialog.show();

        Button b = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (b != null) {
            b.setTextColor(Color.rgb(0x35,0x63,0xFF));
        }
    }


    private void gotoWeChatLogin() {

        Intent intent =  new Intent(this, WeChatLoginActivity.class);
        startActivity(intent);
    }

    private void gotoSignupProfile() {

        _user.setName(ui_edt_nickname_inLoginActivity.getText().toString().trim());

        Intent intent = new Intent(this, SignupProfileActivity.class);
        intent.putExtra(Constants.USER, _user);

        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }



    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.lyt_back_inLoginActivity:
                gotoBack();
                break;
            case R.id.txv_explain_inLoginActivity:
                showExplain();
                break;
            case R.id.imv_we_chat:
                ui_we_chat.setImageResource(R.drawable.ic_login_weixin_pressed);
                gotoWeChatLogin();
                break;
            case R.id.txv_next_inLoginActivity:
                gotoSignupProfile();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ui_we_chat.setImageResource(R.drawable.ic_login_weixin_normal);
    }
}
