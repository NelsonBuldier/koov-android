package com.dating.koov.main;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class MeActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_InstantVideo, ui_lyt_Message, ui_lyt_Contact, ui_lyt_Nearby;

    ImageView ui_imv_Me;

    TextView ui_txv_Me, ui_txv_login_inMeActivity, ui_txv_register_inMeActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_Me = (ImageView)findViewById(R.id.imv_Me);
        ui_imv_Me.setImageResource(R.drawable.ic_nav_5_active);

        ui_txv_Me = (TextView)findViewById(R.id.txv_Me);
        ui_txv_Me.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Message = (LinearLayout)findViewById(R.id.lyt_Message);
        ui_lyt_Message.setOnClickListener(this);

        ui_lyt_Contact = (LinearLayout)findViewById(R.id.lyt_Contact);
        ui_lyt_Contact.setOnClickListener(this);

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Nearby = (LinearLayout)findViewById(R.id.lyt_Nearby);
        ui_lyt_Nearby.setOnClickListener(this);

        ui_txv_login_inMeActivity = (TextView)findViewById(R.id.txv_login_inMeActivity);
        ui_txv_login_inMeActivity.setOnClickListener(this);

        ui_txv_register_inMeActivity = (TextView)findViewById(R.id.txv_register_inMeActivity);
        ui_txv_register_inMeActivity.setOnClickListener(this);

    }

    private void gotoNearbyActivity() {

        Intent intent = new Intent(this, NearbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoInstantVideoActivity() {

        Intent intent  = new Intent(this, InstantVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMessageActivity() {

        Intent intent = new Intent(this, MessageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoContactActivity() {

        Intent intent = new Intent(this, ContactActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoSignupAccountActivity() {

        Intent intent =  new Intent(this, SignupAccountActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void gotoLoginActivity() {

        Intent intent =  new Intent(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.lyt_InstantVideo:
                gotoInstantVideoActivity();
                break;
            case R.id.lyt_Message:
                gotoMessageActivity();
                break;
            case R.id.lyt_Contact:
                gotoContactActivity();
                break;
            case R.id.lyt_Nearby:
                gotoNearbyActivity();
                break;
            case R.id.txv_login_inMeActivity:
                gotoSignupAccountActivity();
                break;
            case R.id.txv_register_inMeActivity:
                gotoLoginActivity();
                break;
        }
    }

}
