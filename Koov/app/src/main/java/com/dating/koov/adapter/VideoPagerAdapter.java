package com.dating.koov.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dating.koov.fragments.NearbyFragment;
import com.dating.koov.fragments.NewestFragment;
import com.dating.koov.fragments.SuggestFragment;
import com.dating.koov.main.InstantVideoActivity;

/**
 * Created by sts on 7/17/2016.
 */
public class VideoPagerAdapter extends FragmentStatePagerAdapter {

    InstantVideoActivity _instantVideoActivity;

    public VideoPagerAdapter(InstantVideoActivity instantVideoActivity, FragmentManager fm) {

        super(fm);
        this._instantVideoActivity = instantVideoActivity;
    }

    @Override
    public int getItemPosition(Object object) {return super.getItemPosition(object);}

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position){
            case 0:
                fragment = new SuggestFragment(_instantVideoActivity);
                break;
            case 1:
                fragment = new NearbyFragment(_instantVideoActivity);
                break;
            case 2:
                fragment = new NewestFragment(_instantVideoActivity);
                break;
        }
        return fragment;
    }



    @Override
    public CharSequence getPageTitle(int position) {
        String title="";
        switch (position){
            case 0:
                title="Suggest";
                break;
            case 1:
                title="Nearby";
                break;
            case 2:
                title="Newest";
                break;
        }

        return title;
    }

    @Override
    public int getCount() {
        return 3;
    }


}
