package com.dating.koov.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;

public class GetPasswordActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_back_inGoBackRegisterActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_back_register);

        loadLayout();
    }

    private void loadLayout() {

        ui_imv_back_inGoBackRegisterActivity = (ImageView)findViewById(R.id.imv_back_inGoBackRegisterActivity);
        ui_imv_back_inGoBackRegisterActivity.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back_inGoBackRegisterActivity:
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                finish();
                break;
        }
    }
}
