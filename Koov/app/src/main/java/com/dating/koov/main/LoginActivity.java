package com.dating.koov.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.commons.Constants;
import com.dating.koov.model.UserEntity;

import java.util.Arrays;

public class LoginActivity extends CommonActivity implements View.OnClickListener {

    ImageView ui_imv_back_inRegisterActivity, ui_imv_WeChat_inRegisterActivity;

    EditText ui_edt_photn_email_inRegisterActivity, ui_edt_password_inRegister;

    TextView ui_txv_register_inAccountActivity, ui_txv_postal_numger_sel_inRegisterActivity, ui_txv_forgot_password;

    String phone_or_email="", password = "";

    UserEntity user = new UserEntity();

    boolean _isFromLogout = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        initValues();

        loadLayout();
    }

    private void initValues() {

        Intent intent =getIntent();
        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void loadLayout() {

        ui_txv_register_inAccountActivity = (TextView)findViewById(R.id.txv_register_inLoginActivity);

        ui_txv_forgot_password = (TextView)findViewById(R.id.txv_forgot_password);
        ui_txv_forgot_password.setOnClickListener(this);

        ui_txv_postal_numger_sel_inRegisterActivity = (TextView)findViewById(R.id.txv_postal_numger_sel_inRegisterActivity);
        ui_txv_postal_numger_sel_inRegisterActivity.setOnClickListener(this);

        ui_imv_back_inRegisterActivity = (ImageView)findViewById(R.id.imv_back_inRegisterActivity);
        ui_imv_back_inRegisterActivity.setOnClickListener(this);

        ui_imv_WeChat_inRegisterActivity = (ImageView)findViewById(R.id.imv_WeChat_inRegisterActivity);
        ui_imv_WeChat_inRegisterActivity.setOnClickListener(this);

        ui_edt_photn_email_inRegisterActivity = (EditText)findViewById(R.id.edt_phone_email_inRegisterActivity);
        ui_edt_photn_email_inRegisterActivity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                phone_or_email = ui_edt_photn_email_inRegisterActivity.getText().toString().trim();

                if (phone_or_email.length() == 0 || password.length() == 0 ){

                    ui_txv_register_inAccountActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_register_inAccountActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_register_inAccountActivity.setOnClickListener(null);

                }else {

                    ui_txv_register_inAccountActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_register_inAccountActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_register_inAccountActivity.setOnClickListener(LoginActivity.this);
                }

            }
        });

        ui_edt_password_inRegister = (EditText)findViewById(R.id.edt_password_inRegister);
        ui_edt_password_inRegister.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable editable) {

                password = ui_edt_password_inRegister.getText().toString().trim();
                if (phone_or_email.length() == 0 || password.length() == 0 ){

                    ui_txv_register_inAccountActivity.setTextColor(Color.rgb(0xe6, 0xe6, 0xe6));
                    ui_txv_register_inAccountActivity.setBackgroundResource(R.drawable.bg_txv_next_button_normal);
                    ui_txv_register_inAccountActivity.setOnClickListener(null);

                }else {

                    ui_txv_register_inAccountActivity.setTextColor(Color.rgb(0xff, 0xff, 0xff));
                    ui_txv_register_inAccountActivity.setBackgroundResource(R.drawable.bg_txv_next_button_active);
                    ui_txv_register_inAccountActivity.setOnClickListener(LoginActivity.this);
                }
            }
        });

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_photn_email_inRegisterActivity.getWindowToken(), 0);
                return false;
            }
        });


    }

    private void gotoWeChatActivity() {

        Intent intent =  new Intent(this, WeChatLoginActivity.class);
        startActivity(intent);
    }

    private void showPostalNumber() {

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Select postal number");

        final String[] postal_number_array = getResources().getStringArray(R.array.postal_number);

        adb.setItems(postal_number_array, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {

                String clickedItemValue = Arrays.asList(postal_number_array).get(which);
                StringBuilder postal = new StringBuilder();

                for (int i= 0; i<clickedItemValue.length(); i++){

                    char c= clickedItemValue.charAt(i);
                    if (Character.isDigit(c)){
                        postal.append(c);
                    }
                }
                ui_edt_photn_email_inRegisterActivity.setText("+"+postal);
            }
        });

        adb.show().getWindow().setLayout(650, 1000);
    }

    private void gotoForgotActivity() {

        Intent intent =  new Intent(this, ForgotActivity.class);

        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private boolean checkInputValue() {

        String phone_number = ui_edt_photn_email_inRegisterActivity.getText().toString();

        if (isNumeric(phone_number) || phone_number.contains("@gmail.com") ||  phone_number.contains("@hotmail.com")) {

            return true;

        }
        showAlertDialog("Input phone number or email correctly");
        return false;
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private void gotoLoginSecondActivity() {

    }

        @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imv_back_inRegisterActivity:

                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                break;

            case R.id.imv_WeChat_inRegisterActivity:

                ui_imv_WeChat_inRegisterActivity.setImageResource(R.drawable.ic_login_weixin_pressed);
                gotoWeChatActivity();
                break;

            case R.id.txv_register_inLoginActivity:

                if (checkInputValue()) {
                    gotoLoginSecondActivity();
                }

                this.showToast("fialed sever connecting");
                break;

            case R.id.txv_postal_numger_sel_inRegisterActivity:

                showPostalNumber();
                break;

            case R.id.txv_forgot_password:

                gotoForgotActivity();
                break;
        }
    }

    @Override
    protected void onResume() {

        ui_imv_WeChat_inRegisterActivity.setImageResource(R.drawable.ic_login_weixin_normal);

        super.onResume();
    }
}
