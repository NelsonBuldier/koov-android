package com.dating.koov.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.dating.koov.R;
import com.dating.koov.adapter.NewestAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.InstantVideoActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/2016.
 */
public class NewestFragment extends BaseFragment{

    InstantVideoActivity instantVideoActivity;

    private SwipeRefreshLayout ui_Swipe_Refresh_inNewestFragment;

    private GridView  ui_gridView_inNewestFragment;

    private NewestAdapter newestAdapter;

    private ArrayList<MyItem> arItem;

    public NewestFragment(InstantVideoActivity context){

        this.instantVideoActivity = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_newest, container, false);

        ui_gridView_inNewestFragment = (GridView) view.findViewById(R.id.gridView_inNewestFragment);

        ui_Swipe_Refresh_inNewestFragment = (SwipeRefreshLayout) view.findViewById(R.id.Swipe_Refresh_inNewestFragment);
        ui_Swipe_Refresh_inNewestFragment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ui_Swipe_Refresh_inNewestFragment.setRefreshing(false);
                    }
                }, 1000);
            }
        });

        arItem = new ArrayList<>();

        MyItem mi;
        mi = new MyItem(R.drawable.ic_welcome_photo_11, "Jacson"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_12, "Sims"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_13, "Jerry"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_14, "Star"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_15, "aaa"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_16, "bbb"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_17, "ccc"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_18, "ddd"); arItem.add(mi);

        newestAdapter = new NewestAdapter(instantVideoActivity, arItem);
        ui_gridView_inNewestFragment.setAdapter(newestAdapter);

        return view;
    }



    public class MyItem {

        public int Icon;
        public String Name;

        MyItem(int aIcon, String aName){
            Icon = aIcon;
            Name = aName;
        }
    }

}