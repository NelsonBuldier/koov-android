package com.dating.koov.base;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.view.View;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by HGS on 12/11/2015.
 */

public abstract class BaseFragment extends Fragment {

    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public abstract void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset);
//
//    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
//    public abstract void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId);

}
