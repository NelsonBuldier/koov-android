package com.dating.koov.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.dating.koov.R;
import com.dating.koov.adapter.PeopleAdapter;
import com.dating.koov.base.BaseFragment;
import com.dating.koov.main.NearbyActivity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PeopleFragment extends BaseFragment {

    private NearbyActivity nearbyActivity;
    private ListView ui_listPeople;
    private SwipeRefreshLayout ui_RefreshLayout;

    private PeopleAdapter _adapter;

    private ArrayList<MyItem> arItem;

    private ArrayList<Object> _users = new ArrayList<>();

    public PeopleFragment(NearbyActivity context) {

        this.nearbyActivity = context;
        // Required empty public constructor

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);

        ui_RefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.Swipe_Refresh_inPeople);
        ui_RefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){

            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ui_RefreshLayout.setRefreshing(false);
                    }
                },1000);
            }
        });

        arItem = new ArrayList<>();

        MyItem mi;
        mi = new MyItem(R.drawable.ic_welcome_photo_1, "Jacson"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_2, "Sims"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_3, "Jerry"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_4, "Star"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_5, "aaa"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_6, "bbb"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_7, "ccc"); arItem.add(mi);
        mi = new MyItem(R.drawable.ic_welcome_photo_8, "ddd"); arItem.add(mi);


        _adapter = new PeopleAdapter(nearbyActivity, arItem);

        ui_listPeople = (ListView)view.findViewById(R.id.lst_users_inPeople);
        ui_listPeople.setAdapter(_adapter);

        return view;
    }


    public class MyItem {

        public int Icon;
        public String Name;

        MyItem(int aIcon, String aName){
            Icon = aIcon;
            Name = aName;
        }
    }
}
