package com.dating.koov.details;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.adapter.ViewPagerAdapter;
import com.dating.koov.base.CommonActivity;

import java.util.ArrayList;

public class UserUploadImagetActivity extends CommonActivity{

    ViewPagerAdapter mViewPagerAdapter;

    ViewPager viewPager;

    TextView ui_txv_upload_img_cnt;

    ArrayList<Integer> mImages = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_upload_imaget);

        loadlayout();
    }

    private void loadlayout() {

        ui_txv_upload_img_cnt = (TextView)findViewById(R.id.txv_upload_img_cnt);

        downloadDatas();

        mViewPagerAdapter = new ViewPagerAdapter(this, mImages);

        viewPager = (ViewPager)findViewById(R.id.vip_upload_img);

        viewPager.setAdapter(mViewPagerAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                String str = String.valueOf(position+1) + "/" + String.valueOf(mImages.size());
                CharSequence chr = str;
                ui_txv_upload_img_cnt.setText(chr);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    private void downloadDatas() {

        mImages.add(R.drawable.th1);
        mImages.add(R.drawable.th2);
        mImages.add(R.drawable.th3);
        mImages.add(R.drawable.th4);
        mImages.add(R.drawable.th5);
    }
}
