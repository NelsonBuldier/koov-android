package com.dating.koov.commons;

/**
 * Created by HGS on 12/11/2015.
 */
public class Constants {

    public static final int SPLASH_TIME = 1000;

    public static final String USER = "user";

    public static final int DATE_DIALOG = 1;

    public static final int PROFILE_IMAGE_SIZE = 256;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;

    public static final String KEY_LOGOUT = "logout";


}
