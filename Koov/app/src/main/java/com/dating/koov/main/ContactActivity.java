package com.dating.koov.main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.base.CommonActivity;
import com.dating.koov.preference.PrefConst;
import com.dating.koov.preference.Preference;

public class ContactActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_Nearby, ui_lyt_InstantVideo, ui_lyt_Message, ui_lyt_Me;

    ImageView ui_imv_contact;

    TextView ui_txv_contact, ui_txv_Login_inContactActivity, ui_txv_register_inAccountActivity;

    TextView ui_txv_logout;

    Boolean success_flag = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        success_flag = Preference.getInstance().getValue(this, PrefConst.LOGIN_SUCCESS, false);

        if (success_flag){

            setContentView(R.layout.activity_login_success);
//            loadLayoutSuccess();
        }else {

            setContentView(R.layout.activity_contact);
//            loadLayoutUnsuccess();
        }
        loadLayoutUnsuccess();

    }

    private void loadLayoutSuccess() {

        ui_imv_contact = (ImageView)findViewById(R.id.imv_Contacts);
        ui_imv_contact.setImageResource(R.drawable.ic_nav_4_active);

        ui_txv_contact = (TextView)findViewById(R.id.txv_contact);
        ui_txv_contact.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

        ui_lyt_Nearby = (LinearLayout)findViewById(R.id.lyt_Nearby);
        ui_lyt_Nearby.setOnClickListener(this);

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Message = (LinearLayout)findViewById(R.id.lyt_Message);
        ui_lyt_Message.setOnClickListener(this);

        ui_lyt_Me = (LinearLayout)findViewById(R.id.lyt_Me);
        ui_lyt_Me.setOnClickListener(this);

        ui_txv_logout = (TextView)findViewById(R.id.txv_logout);
        ui_txv_logout.setOnClickListener(this);


    }

    private void loadLayoutUnsuccess() {

        ui_imv_contact = (ImageView)findViewById(R.id.imv_Contacts);
        ui_imv_contact.setImageResource(R.drawable.ic_nav_4_active);

        ui_txv_contact = (TextView)findViewById(R.id.txv_contact);
        ui_txv_contact.setTextColor(Color.rgb(0xd7, 0xd2,0xd3));

        ui_lyt_Nearby = (LinearLayout)findViewById(R.id.lyt_Nearby);
        ui_lyt_Nearby.setOnClickListener(this);

        ui_lyt_InstantVideo = (LinearLayout)findViewById(R.id.lyt_InstantVideo);
        ui_lyt_InstantVideo.setOnClickListener(this);

        ui_lyt_Message = (LinearLayout)findViewById(R.id.lyt_Message);
        ui_lyt_Message.setOnClickListener(this);

        ui_lyt_Me = (LinearLayout)findViewById(R.id.lyt_Me);
        ui_lyt_Me.setOnClickListener(this);

        FragmentManager manager = getSupportFragmentManager();



        if (success_flag) {

            ui_txv_logout = (TextView)findViewById(R.id.txv_logout);
            ui_txv_logout.setOnClickListener(this);
        }else {

            ui_txv_Login_inContactActivity = (TextView) findViewById(R.id.txv_Login_inContactActivity);
            ui_txv_Login_inContactActivity.setOnClickListener(this);

            ui_txv_register_inAccountActivity = (TextView) findViewById(R.id.txv_register_inAccountActivity);
            ui_txv_register_inAccountActivity.setOnClickListener(this);
        }
    }

    private void gotoNearbyActivity() {

        Intent intent = new Intent(this, NearbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoInstantVideo() {

        Intent intent = new Intent(this, InstantVideoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMessageActivity() {

        Intent intent = new Intent(this, MessageActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoMeActivity() {

        Intent intent = new Intent(this, MeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void gotoSignupAccountActivity() {

        Intent intent = new Intent(this, SignupPhoneNumberActivity.class);

//        Intent intent =  new Intent(this, SignupAccountActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    private void gotoLoginActivity() {

        Intent intent =  new Intent(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

    }

    private void gotoLogout() {

        Preference.getInstance().put(this, PrefConst.LOGIN_SUCCESS, false);

        Intent intent = new Intent(this, NearbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.lyt_Nearby:
                gotoNearbyActivity();
                break;
            case R.id.lyt_InstantVideo:
                gotoInstantVideo();
                break;
            case R.id.lyt_Message:
                gotoMessageActivity();
                break;
            case R.id.lyt_Me:
                gotoMeActivity();
                break;
            case R.id.txv_Login_inContactActivity:
                gotoSignupAccountActivity();
                break;
            case R.id.txv_register_inAccountActivity:
                gotoLoginActivity();
                break;
            case R.id.txv_logout:
                gotoLogout();
                break;
        }
    }

}
