package com.dating.koov.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dating.koov.R;
import com.dating.koov.fragments.NearbyFragment;
import com.dating.koov.main.InstantVideoActivity;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/2016.
 */
public class NearbyAdapter extends BaseAdapter{

    private InstantVideoActivity _context;
    ArrayList<NearbyFragment.MyItem> _users;

    public NearbyAdapter(InstantVideoActivity context, ArrayList<NearbyFragment.MyItem> users){

        super();
        this._context = context;
        this._users = users;
    }

    @Override
    public int getCount() {
        return _users.size();
    }

    @Override
    public Object getItem(int position) {        return _users.get(position).Name;    }

    @Override
    public long getItemId(int position) {return position;}

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        ViewHolder holder =  new ViewHolder();

        if (convertView == null){

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.gridview_item_in_nearby_fragment, viewGroup, false);

            holder.imvPhoto = (ImageView) convertView.findViewById(R.id.imv_photo_gridview_item_in_nearby_fragment);
            holder.imvPhoto.setImageResource(_users.get(position).Icon);

            holder.txvName1 = (TextView)convertView.findViewById(R.id.txv_name1_gridview_item_in_nearby_fragment);
            holder.txvName1.setText(_users.get(position).Name);

            holder.txvName2 = (TextView)convertView.findViewById(R.id.txv_name2_gridview_item_in_nearby_fragment);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }

        return convertView;
    }

    private class ViewHolder {
        public ImageView imvPhoto;
        public TextView txvName1;
        public TextView txvName2;
    }
}
