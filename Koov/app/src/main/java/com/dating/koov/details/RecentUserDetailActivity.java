package com.dating.koov.details;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.dating.koov.R;
import com.dating.koov.base.BaseActivity;
import com.dating.koov.base.CommonActivity;

public class RecentUserDetailActivity extends CommonActivity implements View.OnClickListener {

    LinearLayout ui_lyt_upload_image_inDetail;

    ImageView ui_imv_back;

    EditText ui_edt_input_inDetail;

    ListView ui_lst_user_comet_inDetail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_user_detail);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        loadlayout();
    }

    private void loadlayout() {

        ui_imv_back = (ImageView)findViewById(R.id.imv_back);
        ui_imv_back.setOnClickListener(this);

        ui_lyt_upload_image_inDetail = (LinearLayout)findViewById(R.id.lyt_upload_image_inDetail);
        ui_lyt_upload_image_inDetail.setOnClickListener(this);

        ui_edt_input_inDetail = (EditText)findViewById(R.id.edt_input_inDetail);

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ui_edt_input_inDetail.getWindowToken(), 0);
                return false;
            }
        });
    }

    private void gotoUserUploadImageAvtivity() {

        Intent intent = new Intent(this, UserUploadImagetActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_back:
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                break;
            case R.id.lyt_upload_image_inDetail:
                gotoUserUploadImageAvtivity();
                break;
        }

    }


}
