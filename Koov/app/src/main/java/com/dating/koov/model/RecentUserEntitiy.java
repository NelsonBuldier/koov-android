package com.dating.koov.model;

import android.widget.ImageView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sts on 7/24/2016.
 */
public class RecentUserEntitiy implements Serializable {

    int _idx = 0;
    String _name = "";
    String _photo_url = "";
    String _sex_img_url = "";
    String _year_img_url = "";
    String _content = "";
    ArrayList<String> _upload_img_url = new ArrayList<>();
    String _location_info_img_url = "";
    String _info = "";
    String _distance = "";

    public String get_content() {
        return _content;
    }

    public String get_distance() {
        return _distance;
    }

    public int get_idx() {
        return _idx;
    }

    public String get_info() {
        return _info;
    }

    public String get_location_info_img_url() {
        return _location_info_img_url;
    }

    public String get_name() {
        return _name;
    }

    public String get_sex_img_url() {
        return _sex_img_url;
    }
    public String get_year_img_url() {
        return _year_img_url;
    }
    public String get_photo_url() {
        return _photo_url;
    }

    public ArrayList<String> get_upload_img_url() {
        return _upload_img_url;
    }

    public void set_upload_img_url(ArrayList<String> _upload_img_url) {
        this._upload_img_url = _upload_img_url;
    }

    public void set_content(String _content) {
        this._content = _content;
    }

    public void set_distance(String _distance) {
        this._distance = _distance;
    }

    public void set_idx(int _idx) {
        this._idx = _idx;
    }

    public void set_info(String _info) {
        this._info = _info;
    }

    public void set_location_info_img_url(String _location_info_img_url) {
        this._location_info_img_url = _location_info_img_url;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public void set_sex_img_url(String _sex_img_url) {
        this._sex_img_url = _sex_img_url;
    }
    public void set_year_img_url(String _year_img_url) {
        this._year_img_url = _year_img_url;
    }

    public void set_photo_url(String _photo_url) {
        this._photo_url = _photo_url;
    }
}

